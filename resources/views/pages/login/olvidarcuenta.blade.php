@extends('layout.admin2')
@section('contenido')
<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login101">
				<form class="login100-form2 validate-form">
					<div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
						<span class="label-input100">Email:</span>
						<input class="input100" type="text" name="username" placeholder="Ingrese Correo Electronico">
						<span class="focus-input100"></span>
					</div>Para poder recuperar tu cuenta,
					por favor ingrese un correo electronico.
					<span class="label-input100">
						 
					</span>
					
				
					<div class="container-login100-form-btn">
						<button class="login100-form-btn fab fa-keycdn">
							Enviar enlace de recuperacion
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection