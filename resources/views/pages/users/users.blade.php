@extends('layout.admin2')
@section('contenido')
<div class="wrap-login102" style="align:center !important; ">
				<div class="login100-form-title" style="background-image: url(images/registrarpanakunacabezal.png)">
					<span class="login100-form-title-1">Registrar Usuario 
						
					</span>
				</div>
</div>
	<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box" style="border-top: 3px solid #fff">
                <div class="box-header with-border" style="padding: 10px !important">
                    <h4>
                        <strong style="font-weight: 400">
                            <i class="fas fa-user-plus"></i></i>
                        </strong>
                    </h4>
                    @if(count($errors)>0)
                    <div class="alert-alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach

                        </ul>
                    </div>
                    @endif
                </div>
                <!-- mantener valores al -->
                <div class="box-body bg-gray-c">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="panel panel-default panel-shadow">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="" class="control-label" style="font-size: 13px;color: #676a6c">
                                            Registrar
                                        </label>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="nombres">Nombres</label>
                                                <input type="text" name="nombre" id="nombre" class="form-control"
                                                    placeholder="Ingrese el Nombre">
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="nombres">Apellidos</label>
                                                <input type="text" name="apellido" id="apellido" class="form-control"
                                                    placeholder="Ingrese Apellido">
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="nombres">DNI</label>
                                                <input type="text" name="numero_documento" id="documento" class="form-control"
                                                    placeholder="Ingrese DNI">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                        <div class="form-group">
													<label>
														Fecha de Nacimiento
													</label>
													<div class="input-group date">
													    <div class="input-group-addon">
															<i class="far fa-calendar-alt"></i>
														</div>
												<input  id="fecha_nacimiento" type="date" class="form-control pull-right" name="fecha_nacimiento">	
												    </div>
											    </div>										
											</div>		
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="nombres">E-mail</label>
                                                <input type="text" name="email" id="email" class="form-control"
                                                    placeholder="Ingrese E-mail">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="nombres">Telefono</label>
                                                <input type="text" name="numero" id="numero"
                                                    class="form-control" placeholder="Ingrese  telefonico">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row"> 
                                    <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="nombres">E-mail</label>
                                                <input type="text" name="email" id="email" class="form-control"
                                                    placeholder="Ingrese E-mail">
                                            </div>
                                        </div>
                                    <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="nombres">Password</label>
                                                <input type="password" name="password" id="password"
                                                    class="form-control" placeholder="Ingrese  telefonico">
                                            </div>
                                        </div>           
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Tipo Telefono</label>
                                                <select name="telefono" id="telefono" class="form-control">
                                                    <option>Seleccione</option>
                                                    <option value="1">Movil</option>
                                                    <option value="2">Fijos</option>
                                                    <option value="3">Otros</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Operador</label>
                                                <select name="operador" id="operador" class="form-control">
                                                    <option>Seleccione</option>
                                                    <option value="1">Bitel</option>
                                                    <option value="2">Claro</option>
                                                    <option value="3">Entel</option>
                                                    <option value="3">Mosvitar</option>
                                                </select>
                                            </div>
                                        </div>    

                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <label for="nombres">Direccion</label>
                                                <input type="text" name="descuento" id="descuento" class="form-control"
                                                    placeholder="Ingrese Descripcion del producto">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                    
                                    <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Departamento</label>
                                            <select  class="form-control selectpicker" id="departamento" name="departamento" data-live-search="true">
                                            <option value="" disabled="" selected="">Seleccione</option>
                                            @foreach($departamento as $depa)                
                                            <option value="{{$depa->iddepartamento}}">{{$depa->departamento}}</option>
                                            @endforeach  
                                            </select>   
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                    <label class="control-label">Provincia</label>                                  
                                                <select  class="form-control selectpicker" id="provincia" name="provincia" data-live-search="true">
                                                    <option value="" disabled="" selected="">Seleccione</option>
                                                </select>   
                                                </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                    <label class="control-label">Distrito</label>                                  
                                                <select  class="form-control selectpicker" id="distrito" name="distrito" data-live-search="true">
                                                    <option value="" disabled="" selected="">Seleccione</option>
                                                </select>   
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label for="" class="control-label" style="font-size: 13px;color: #676a6c">
                                                <div style="margin-top: 20px" class="from-group ">
                                                    <button id="save" class="btn btn-primary btn-sm" type="button"><i
                                                            class="far fa-save"></i> Registrarse</button>
                                                    <button class="btn btn-danger" type="reset">Limpiar</button>
                                                </div>
                                        </div>
                                    </div>
                                    <script>                                     
                                    </script>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
</section>
@push('scripts')
<script type="text/javascript">

    // $('#departamento').click(function(){
    //         console.log('departamento');
    // });
    // $("#departamento").change(console.log('entre'));
    var selectDepartamento = document.getElementById('departamento');
    selectDepartamento.addEventListener('change',function(){
        var selectedOption = this.options[selectDepartamento.selectedIndex];
        console.log(selectedOption.value + ': ' + selectedOption.text);
        var id=selectedOption.value;
        console.log(id);
        provincia(id);
        
    });
    var selectProvincia = document.getElementById('provincia');
    selectProvincia.addEventListener('change',function(){
        var selectedOption = this.options[selectProvincia.selectedIndex];
        console.log(selectedOption.value + ': ' + selectedOption.text);
        var id=selectedOption.value;
        console.log(id);
        distrito(id);        
    });
    function provincia(idDepartamento){
        console.log(idDepartamento,'-----');
      $.ajax({
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data:{departamento:idDepartamento}, //datos que se envian a traves de ajax
            url:'departamento', //archivo que recibe la peticion
            type:'post', //método de envio
            dataType:"json",//tipo de dato que envio 
            beforeSend: function () {
                console.log('procesando');
                // $("#resultado").html("Procesando, espere por favor...");
            },
            success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                if(response.veri==true){
                    // var urlBase=window.location.origin;
                    // var url=urlBase+'/'+response.data;
                    // document.location.href=url;
                    var provincia=response.provincia;
                    var va;
                    // console.log(response.provincia,response.veri);
                    va='<option value="" disabled="" selected="">Seleccione</option>'
                    for(const i in provincia){
                        va+='<option value="'+provincia[i]['id']+'">'+provincia[i]['nombre_provincia']+'</option>';                 
                    }
                    $("#provincia").html(va); 
                }else{
                    alert("problemas al enviar la informacion");
                }
            }
        });
    }
    function distrito(idProvincia){
        console.log(idProvincia,'-----');
      $.ajax({
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data:{Provincia:idProvincia}, //datos que se envian a traves de ajax
            url:'distrito', //archivo que recibe la peticion
            type:'post', //método de envio
            dataType:"json",//tipo de dato que envio 
            beforeSend: function () {
                console.log('procesando');
                // $("#resultado").html("Procesando, espere por favor...");
            },
            success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                if(response.veri==true){
                    // var urlBase=window.location.origin;
                    // var url=urlBase+'/'+response.data;
                    // document.location.href=url;
                    var distrito=response.distrito;
                    var va;
                    // console.log(response.distrito,response.veri);
                    va='<option value="" disabled="" selected="">Seleccione</option>'
                    for(const i in distrito){
                        va+='<option value="'+distrito[i]['id']+'">'+distrito[i]['nombre_distrito']+'</option>';      
                    }
                    $("#distrito").html(va); 
                }else{
                    alert("problemas al enviar la informacion");
                }
            }
        });
    }

 $('#save').click(function(){
     saveRegistrar();
 });

 function saveRegistrar(){

     let nombre= $('#nombre').val();
     let apellidos= $('#apellido').val();
     let numero_documento=$('#numero_documento').val();
 }


</script>
@endpush
@endsection