@extends('layout.admin2')
@section('contenido')
<div class="banner-bootom-w3-agileits py-5">
		<div class="container py-xl-4 py-lg-2">
			<!-- tittle heading -->
			<h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
				<span>V</span>ITALINTI
				<span>G</span>OLDEN MIX</h3>
			<!-- //tittle heading -->
			<div class="row">
				<div class="col-lg-5 col-md-8 single-right-left ">
					<div class="grid images_3_of_2">
						<div class="flexslider">
							<ul class="slides">
								<!-- tittle heading
									<li data-thumb="images/FotosDetalle1/foto3.jpg">
									<div class="thumb-image">
										<img src="images/FotosDetalle1/foto3.jpg" data-imagezoom="true" class="img-fluid" alt=""> </div>
								</li>
							 -->	
								<li data-thumb="images/Productos/snacks_saludables/vitalinti_golden_mixback.png">
									<div class="thumb-image">
										<img src="images/Productos/snacks_saludables/vitalinti_golden_mixback.png" data-imagezoom="true" class="img-fluid" alt=""> </div>
								</li>
							<!-- tittle heading
								<li data-thumb="images/FotosDetalle1/fotos1.jpg">
									<div class="thumb-image">
										<img src="images/FotosDetalle1/fotos1.jpg" data-imagezoom="true" class="img-fluid" alt=""> </div>
								</li>-->
							</ul>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>

				<div class="col-lg-7 single-right-left simpleCart_shelfItem">
					<h3 class="mb-3">VITALINTI - GOLDEN MIX</h3>
					<p class="mb-3">
						<span class="item_price">S/13,00 c/u</span>
						<del class="mx-2 font-weight-light">S/0.00 c/u</del>
						<label>Gratis el envio</label>
					</p>
					<!--<div class="single-infoagile">
						<ul>
							<li class="mb-3">
								Cash on Delivery Eligible.
							</li>
							<li class="mb-3">
								Shipping Speed to Delivery.
							</li>
							<li class="mb-3">
								EMIs from $655/month.
							</li>
							<li class="mb-3">
								Bank OfferExtra 5% off* with Axis Bank Buzz Credit CardT&C
							</li>
						</ul>
					</div>-->
					
					<div class="product-single-w3l">
						<p class="my-3">
							<i class="far fa-hand-point-right mr-2"></i>
							<label>Detalle Producto</label></p>
						<ul>
								Un delicioso snack que contiene :
							
							<li class="mb-1">
								Sacha Inchi Nibs de Cacao
							</li>
							<li class="mb-1">
								Aguaymanto deshidratado
							</li>
							<!--<li class="mb-1">
								3300 mAh Battery
							</li>
							<li class="mb-1">
								Exynos 7870 Octa Core 1.6GHz Processor
							</li>-->
						</ul>
						<p class="my-sm-4 my-3">
							<i class="fas fa-retweet mr-3"></i>Net banking & Credit/ Debit/ ATM card
						</p>
					</div>
					<div class="occasion-cart">
						<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
							<form action="#" method="post">
								<fieldset>
									<input type="hidden" name="cmd" value="_cart" />
									<input type="hidden" name="add" value="1" />
									<input type="hidden" name="business" value=" " />
									<input type="hidden" name="item_name" value="Samsung Galaxy J7 Prime" />
									<input type="hidden" name="amount" value="200.00" />
									<input type="hidden" name="discount_amount" value="1.00" />
									<input type="hidden" name="currency_code" value="USD" />
									<input type="hidden" name="return" value=" " />
									<input type="hidden" name="cancel_return" value=" " />
									<input type="submit" name="submit" value="Añadir al Carro" class="button" />
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection