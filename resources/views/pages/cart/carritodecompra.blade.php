@extends('layout.admin')
@section('contenido')
<div class="privacy py-sm-5 py-4">
		<div class="container py-xl-4 py-lg-2">
			<!-- tittle heading --> 
			@if(isset($cart) && isset($total) )
		
			<h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
				<span>C</span>heckout
			</h3>
			<!-- //tittle heading -->
			<div class="checkout-right">
				<h4 class="mb-sm-4 mb-3">tu carrito de compras tiene:
					
				</h4>
				<div class="table-responsive">
					<table class="timetable_sub">
						<thead>
							<tr>
								<th>Producto</th>
								<th>precio</th>
								<th>cantidad</th>
								<th>Subtotal</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($cart as $c)
							<tr class="rem1">
								{{-- <td class="invert"></td> --}}
								<td class="invert-image">
										{{$c->nombre}}
								</td>
								<td class="invert">
										{{$c->precio}}
									
								</td>
								<td class="invert">
										<input type="number" 
										min="1" 
										max="{{$c->cantidad}}" 
										value="{{$c->cant}}" 
										id="paquete_{{ $c->id }}">
										{{-- <a href="#" 
										class="btn btn-warning btn-update-item" 
										data-href="{{ route('cart-update',$c->id) }}" 
										data-id="{{ $c->id }}"> <i class="fa fa-refres">)</i> </a> --}}
								</td>
								<td class="invert">
										{{ number_format($c->subto,2) }}
								</td>								
								<td class="invert">
									<a href="{{route('cart-delete',$c->id)}}" class="btn btn-default">
										<i class="fas fa-trash"></i>
									</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					<h3 class="text-xs-right"> 
							<span class="label label-success">
									Total: ${{number_format($total,2)}}
								</span></h3>
				</div>

			</div>
			<div class="row mt-5">
					@if(isset(Auth::user()->id))
						<div class="col-md-3">
					<!--	<div class="checkout-right-basket">
							<a href="payment.html">Make a Payment
								<span class="far fa-hand-point-right"></span>
							</a>
						</div>-->	
						<a href="https://www.paypal.com/signin?returnUri=https%3A%2F%2Fwww.paypal.com%2Fcgi-bin%2Fwebscr%3fcmd%3d_account"  class="btn btn-outline-primary" role="button">
						<span>
							<img src="{{asset('webfonts/cash.svg')}}" alt="" width="50px" height="30px">
							Pagar con PayPal
						</a>
					</div>
					@else
					<div class="col-md-3">
						<button class="btn btn-outline-default">
							Ingrese para poder realizar el pago
						</button>
					</div>
					@endif

					<div class="col-md-3">
							<a href="{{ route('cart-trash') }}"  class="btn btn-outline-danger" role="button">
						<span>
							<img src="{{asset('webfonts/delete.svg')}}" alt="" width="50px" height="30px">
							Vaciar el carro 
						</a>
					</div>

					<div class="col-md-3">
							<a href="{{URL::to('/')}}" class="btn btn-outline-success" role="button">
							<span>
								<img src="{{asset('webfonts/shopping-bag.svg')}}" alt="" width="50px" height="30px">
								Seguir Comprando
							</span>
						</a>
					</div>
		</div>					
			@else
						<div class="container">
						<div class="row">
						<div class="col-md-12">
						<h3><span class="label label-warning">Agrear productos al Carrito<button class="btn btn-outline-default">
									<a href="{{URL::to('/galeria')}}">Agregar</a> 
								</button> </span> </h3>
						</div></div></div>
							
						@endif
		</div>
	</div>

<script>

document.getElementById('pidMarca');
function aumentaPrecio(){
	var cant
	var Precio
	var total
	



	}


</script>
@endsection

