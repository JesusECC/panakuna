@extends('layout.admin')

@section('contenido')

<div class="agileinfo-ads-display col-lg-12">
					<div class="wrapper">
						<!-- first section -->
						<div class="product-sec1 px-sm-4 px-3 py-sm-5  py-3 mb-4">
							<h3 class="heading-tittle text-center font-italic">SNACKS SALUDABLES</h3>
							<div class="row">
								<!-- first producto uno  -->
								@foreach ($productos as $producto)
								<div class="col-md-4 product-men mt-5">
										<div class="men-pro-item simpleCart_shelfItem">
											<div class="men-thumb-item text-center">
											<img src="{{ $producto->nombre_imagen}}" alt="">
												
												{{-- <div class="men-cart-pro">
													<div class="inner-men-cart-pro">
														<a href="DetallePlato1.html" class="link-product-add-cart">{{ $producto->descripcion }}</a>
													</div>
												</div> --}}
											</div>
											<div class="item-info-product text-center border-top mt-4">
												<h4 class="pt-1">
												{{-- <a href="DetallePlato1.html">{{ $producto->nombre }}</a> --}}
												{{ $producto->nombre }}
												</h4>
												<div class="info-product-price my-2">
													<span class="item_price">{{  $producto->precio }}</span>
													{{-- <del>{{ $producto->precio +  ($producto->precio - (($producto->descuento/100) * $producto->precio))}}</del> --}}
													<del>{{ $producto->descuento }}</del>
												</div>
												<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
													{{-- <form action="#" method="post">
														<fieldset>
															<input type="hidden" name="cmd" value="_cart" />
															<input type="hidden" name="add" value="1" />
															<input type="hidden" name="business" value=" " />
															<input type="hidden" name="item_name" value="{{ $producto->nombre }}" />
															<input type="hidden" name="amount" value="{{  $producto->precio }}" />
															<input type="hidden" name="discount_amount" value="{{$producto->descuento}}" />
															<input type="hidden" name="currency_code" value="USD" />
															<input type="hidden" name="return" value=" " />
															<input type="hidden" name="cancel_return" value=" " />
															<input type="submit" name="submit" value="Añadir al Carro" class="button btn" /> --}}
															<a href="{{route('cart-add',$producto->id )}}"    class=" btn btn-success" >Añadir al Carro</a>
														{{-- </fieldset>
													</form> --}}
												</div>
											</div>
										</div>
									</div>
								@endforeach
							</div>
								
								<!-- fin del producto  -->

					</div>
				</div>
			<
@endsection