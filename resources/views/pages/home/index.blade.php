@extends('layout.admin')
@section('carousel')
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
		<!-- Indicators-->
		<ol class="carousel-indicators">
			<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			
		</ol>
		<div class="carousel-inner" style="margin-letf: 25% !important">
			<div class="carousel-item item1 active">
				<div class="container-fluid">
					<div class="w3l-space-banner">
						<!-- cartel descuento
							<div class="carousel-caption p-lg-5 p-sm-4 p-3">
							<p>Ahora contamos con
								<span></span></p>
							<h3 class="font-weight-bold pt-2 pb-lg-5 pb-4">
								NUEVOS PLATILLOS<span>DE POSTRE</span>
								
							</h3>
							<a class="button2" href="product.html"> Comprar </a>
						</div>
						-->
					</div>
				</div>
			</div>
			<div class="carousel-item item2">
				<div class="container-fluid">
					<div class="w3l-space-banner">
						<!-- cartel descuento 
							<div class="carousel-caption p-lg-5 p-sm-4 p-3">
							<p>
								<span></span></p>
							<h3 class="font-weight-bold pt-2 pb-lg-5 pb-4">NUEVOS PLATILLOS
								<span>de entrada</span>
							</h3>
							<a class="button2" href="product.html"> Comprar </a>
						</div>
						-->
					</div>
				</div>
			</div>
			<div class="carousel-item item3">
				<div class="container-fluid">
					<div class="w3l-space-banner">
						<!---Cartel Descuento
							<div class="carousel-caption p-lg-5 p-sm-4 p-3">
							<p>Descuento del
								<span>10%</span> por la comprar de 50 </p>
							<h3 class="font-weight-bold pt-2 pb-lg-5 pb-4">platillos
								<span>entrada</span>
							</h3>
							<a class="button2" href="product.html"> Comprar </a>
						</div>
						-->
					</div>
				</div>
			</div>
			
		</div>
		<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
@endsection
@section('contenido')
<div class="welcome py-sm-5 py-4">
		<div class="container py-xl-4 py-lg-2">
			<!-- tittle heading -->
			<h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
				<span>N</span>uestra
				<span>E</span>mpresa</h3>
			<!-- //tittle heading -->
			<div class="row">
				<div class="col-lg-6 welcome-left">
					<h3 style="color:#000 !important;">PANAKUNA</h3>
					<h4 class="my-sm-3 my-2"   style="text-align:justify">Nos dedicamos a la distribución de productos gourmet saludables  de alta gama 100% nacionales , l
						os cuales no solo resaltan por sus cualidades como producto , sino también por el trasfondo del mismo ,
						 donde se busca potenciar recursos naturales valiosos y a su vez beneficiar durante sus procesos a todos los implicados , 
						 creando así una cadena de desarrollo de 
						la cual nuestra empresa también forma parte.
					<p>PANAKUNA  brinda un servicio de distribución en el cual ofrecemos a todos nuestros clientes la garantía y seguridad necesaria para que los
						 productos que requieran  lleguen a su destino en perfecto estado , 
						trabajando por un desarrollo y crecimiento mutuo, tanto en beneficio de nuestros 
						clientes como el de los consumidores finales .
					<p>EFICACIA , CALIDAD Y CONFIANZA , son las palabras que nos define como empresa .</h4>
				</div>
				<div class="col-lg-6 welcome-right-top mt-lg-0 mt-sm-5 mt-4">
					<img src="images/panakunafotocentro.jpg" class="img-fluid" alt=" ">
				</div>
			</div>
		</div>
	</div>



    <div class="testimonials py-sm-5 py-4">
		<div class="container py-xl-4 py-lg-2">
			<!-- tittle heading -->
			<h3 class="tittle-w3l text-center text-white mb-lg-5 mb-sm-4 mb-3">
				<span style="color:#1F9B8C !important; ">O</span>bjetivo
				<span style="color:#1F9B8C !important; ">E</span>strategico
			
			<!-- tittle heading -->
			<div class="row gallery-index">
				<div class="col-sm-4 med-testi-grid">
					<div class="med-testi test-tooltip rounded p-4"><h2 style="color:#8D8F6A !important; ">MISION</h2>
						<p>"Difundir el consumo de productos gourmet saludables".</p>
					</div>
			<!--		<div class="row med-testi-left my-5">
						<div class="col-lg-2 col-3 w3ls-med-testi-img">
							<img src="images/user.jpg" alt=" " class="img-fluid rounded-circle" />
						</div>
						<div class="col-lg-10 col-3 med-testi-txt">
							<h4 class="font-weight-bold mb-lg-1 mb-2">MISION</h4>
						</div>
					</div>-->
				</div>
				<div class="col-sm-4 med-testi-grid">
					<div class="med-testi test-tooltip rounded p-4"><h2 style="color:#8D8F6A !important; ">VISION</h2>
						<p>"Ser reconocidos como la principal empresa de asesoría nutricional a nivel nacional, distinguiéndonos por la 
							eficacia de nuestros planes nutricionales y por contribuir en la mejora de la calidad de vida de las personas.".</p>
					</div>
				<!--		<div class="row med-testi-left my-5">
						<div class="col-lg-2 col-3 w3ls-med-testi-img">
							<img src="images/user.jpg" alt=" " class="img-fluid rounded-circle" />
						</div>
						<div class="col-lg-10 col-9 med-testi-txt">
							<h4 class="font-weight-bold mb-lg-1 mb-2">VISION</h4>
						</div>
					</div>-->
				</div>
				<div class="col-sm-4 med-testi-grid">
					<div class="med-testi test-tooltip rounded p-4"><h2 style="color:#8D8F6A !important; ">Valores</h2>
						<p>"Trabajar con un objetivo común, respetando y valorando las diferentes opiniones, fortaleciendo las relaciones interpersonales 
						y priorizando el éxito del equipo en beneficio del resultado por sobre el éxito individual.</p>
					</div>
				<!--	<div class="row med-testi-left mt-sm-5 my-5">
						<div class="col-lg-2 col-3 w3ls-med-testi-img">
							<img src="images/user.jpg" alt=" " class="img-fluid rounded-circle" />
						</div>
						<div class="col-lg-10 col-9 med-testi-txt">
							<h4 class="font-weight-bold mb-lg-1 mb-2">VALORES</h4>
							
						</div>
					</div>-->
				</div>
			<!--	<div class="col-sm-6 med-testi-grid">
					<div class="med-testi test-tooltip rounded p-4">
						<p>"sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
							ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</div>
					<div class="row med-testi-left mt-5">
						<div class="col-lg-2 col-3 w3ls-med-testi-img">
							<img src="images/user.jpg" alt=" " class="img-fluid rounded-circle" />
						</div>
						<div class="col-lg-10 col-9 med-testi-txt">
							<h4 class="font-weight-bold mb-lg-1 mb-2">Jessie</h4>
							<p>fames ac turpis</p>
						</div>
					</div>
				</div>-->
			</div>
		</div>
	</div>
@endsection