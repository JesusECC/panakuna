@extends('pages.dashboard.principal')
@section ('contenido')
<section class="content-header">
    <h1 style="margin-top: 55px;">
        Panel de Administrador
        <small>Version 2.3.0</small>
    </h1>
    <ol class="breadcrumb" style="margin-top: 55px;">
        <li>
            <a href="#">
                <i class="fas fa-dolly"></i> Productos de </a>
        </li>
        <li class="active">Panakuna</li>
        <li>
            <a href="#">
             Nuevo</a>
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box" style="border-top: 3px solid #18A689">
                <div class="box-header with-border" style="padding: 10px !important">
                    <h4>
                        <strong style="font-weight: 400">
                            <i class="fas fa-dolly"></i> Registro Productos Panakuna
                        </strong>
                    </h4>
                    @if(count($errors)>0)
                    <div class="alert-alert-danger">
                        <ul>
                        @foreach($errors->all()as $error)
                                <li>{{$error}}</li>
                        @endforeach
                           
                        </ul>   
                    </div>
                    @endif
                </div>
<!-- mantener valores al -->
<!--{!!Form::open(array('url'=>'dashboard/products','method'=>'POST','autocomplete'=>'off','files'=>'true'))!!}

{{Form::token()}}-->
<div class="box-body bg-gray-c">
                    <div class="row">
                       <div class="col-md-8">
                            <div class="panel panel-default panel-shadow">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="" class="control-label" style="font-size: 13px;color: #676a6c">
                                            Registrar Producto
                                        </label>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="nombres">Codigo</label>
                                                <input type="text" name="codigo" id="codpro" class="form-control" placeholder="Codigo Producto">  
                                            </div>                                              
                                        </div>
                                        
                             <div class="col-sm-4">
                                  <div class="form-group">
                                   <label for="nombres">Nombres</label>
                                        <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Ingrese Nombre Producto"> 
                                   </div>                                                  
                             </div>
                             
                             <div class="col-sm-4">
                                  <div class="form-group">
                                   <label for="nombres">Precio</label>
                                        <input type="number" name="precio" id="precio" class="form-control" placeholder="Ingrese Precio"> 
                                   </div>                                                  
                             </div>
                             </div>

                             <div class="row">
                             <div class="col-sm-4">
                                  <div class="form-group">
                                   <label for="nombres">Peso</label>
                                        <input type="number" name="peso" id="peso" class="form-control" placeholder="Ingrese Nombres"> 
                                   </div>                               
                             </div>   
                            

                             <div class="col-sm-4">
                                  <div class="form-group">
                                   <label for="nombres">Cantidad</label>
                                        <input type="number" name="cantidad" id="cantidad" class="form-control" placeholder="Ingrese Nombres"> 
                                   </div>                               
                             </div>      
                             
								
                                </div>

                            <div class="row">
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                       <label>Tipo  producto</label>
                                         <select name="tipo" id="tipo" class="form-control">
                                                  <option >Seleccione</option>
                                                  <option value="1">Cafe Organico</option>
                                                  <option value="2">Frutas hidratada</option>
                                                  <option value="3">Stack Saludables</option>
                                                  <option value="4">Suplementos</option>
                                         </select>

                                    </div>                                              
                                </div>

                             <div class="col-sm-8">
                                  <div class="form-group">
                                   <label for="nombres">Descripcion</label>
                                        <input type="txt" name="descripcion" id="descripcion" class="form-control" placeholder="Ingrese Descripcion del producto"> 
                                   </div>                               
                             </div>  
                             </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                    <label for="nombres">Descuento</label>
                                            <input type="number" name="descuento" id="descuento" class="form-control" placeholder="Ingrese Descripcion del producto"> 
                                    </div>                               
                                </div> 

                              
                                <div class="col-sm-4">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="" class="control-label" style="font-size: 13px;color: #676a6c">
                                                Imagen Producto Fiemec
                                            </label>
                                            <input  type="file" id="files" name="foto[]" class="form-control">
                                            <br>
                                            <output id="list">
                                            </output>
                                        </div>
                                    </div>
                                </div>
                             </div>

                                        <div class="row">
                                            <div class="col-sm-4"> 
                                                <label for="" class="control-label" style="font-size: 13px;color: #676a6c">                                   
                                                <div style="margin-top: 20px" class="from-group ">
                                                    <button  id="save" class="btn btn-primary btn-sm" type="button"><i class="far fa-save"></i> Guardar</button>
                                                    <button class="btn btn-danger" type="reset">Limpiar</button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                 </div>
                             </div>
                          </div>
                         <!-- {!!Form::close()!!}-->
                      </div>                            
                </div>
</section>




@endsection