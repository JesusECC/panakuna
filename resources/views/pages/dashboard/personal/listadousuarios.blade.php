@extends('pages.dashboard.principal')
@section ('contenido')
<section class="content-header">
	<h1 style="margin-top: 55px;">
		Panel de Administrador
		<small>Version 2.3.0</small>
    </h1>
    <ol class="breadcrumb" style="margin-top: 55px;">
    	<li>
    		<a href="#">
    			<i class="fas fa-dolly"></i> Lista </a>
    	</li>
    	<li class="active">Listado de Usuario </li>
    </ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border" style="padding: 10px !important">
					<h4>
						<strong style="font-weight: 400">
							<i class="fas fa-list-ul"></i> Lista de Personas Registradas
						</strong>
					</h4>
					<div class="ibox-title-buttons pull-right">
						<a href="" style="text-decoration: none !important">
							<button class="btn btn-block btn-success" style="background-color: #18A689 !important;">
								<i class="fas fa-plus-circle"></i> Nuevo Usuario
							</button></a>
					</div>
				</div>
              
				<div class="box-body">
					<table id="example" class="table table-striped table-bordered table-hover" style="width:100%;font-size: 11px !important">
				       <thead>
				            <tr>
				                <th>N° Serie</th>
				                <th>Código Pedido</th>
				                <th>Código</th>
				                <th>Nombre</th>
				                <th>Marca</th>
				                <th>Precio</th>
				                <th>Opciones</th>
				            </tr>
				        </thead>
				        <tbody>
				        
				        </tbody>
				       
    				</table>
    				
				</div>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
</section><!-- /.content -->




@endsection