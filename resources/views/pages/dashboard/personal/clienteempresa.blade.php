@extends('pages.dashboard.principal')
@section ('contenido')
<section class="content-header">
    <h1 style="margin-top: 55px;">
        Panel de Administrador
        <small>Version 2.3.0</small>
    </h1>
    <ol class="breadcrumb" style="margin-top: 55px;">
        <li>
            <a href="#">
                <i class="fas fa-dolly"></i> Productos de </a>
        </li>
        <li class="active">Panakuna</li>
        <li>
            <a href="#">
             Nuevo</a>
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box" style="border-top: 3px solid #18A689">
                <div class="box-header with-border" style="padding: 10px !important">
                    <h4>
                        <strong style="font-weight: 400">
                            <i class="fas fa-dolly"></i> Registro Cliente Empresa
                        </strong>
                    </h4>
                   
                    <div class="alert-alert-danger">
                        <ul>
                        
                                <li></li>
                           
                        </ul>   
                    </div>
               

                </div>
<!-- mantener valores al -->
   
<div class="box-body bg-gray-c">
                    <div class="row">

                       <div class="col-md-12">
                            <div class="panel panel-default panel-shadow">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="" class="control-label" style="font-size: 13px;color: #676a6c">
                                            Registrar Cliente Empresa
                                        </label>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="nombres">Razon Social </label>
                                                <input type="text" name="nombre_RazonSocial" id="nombre_RazonSocial" class="form-control" placeholder="Ingrese la razon social">  
                                            </div>                                              
                                        </div>
                                        
                             <div class="col-sm-4">
                                  <div class="form-group">
                                   <label for="nombres">RUC:</label>
                                        <input type="text" name="ruc" id="ruc" class="form-control" placeholder="Ingrese RUC"> 
                                   </div>                                                  
                             </div>
                             
                             <div class="col-sm-4">
                                  <div class="form-group">
                                   <label for="nombres">Telefono</label>
                                        <input type="text" name="telefono" id="telefono" class="form-control" placeholder="Ingrese Telefono"> 
                                   </div>                                                  
                             </div>
                             </div>

                             <div class="row">
                             <div class="col-sm-4">
                                  <div class="form-group">
                                   <label for="nombres">E-mail</label>
                                        <input type="text" name="email" id="email" class="form-control" placeholder="Ingrese correo electronico"> 
                                   </div>                               
                             </div>  
  
                             <div class="col-sm-4">
                                    <div class="form-group">
                                       <label>Operador</label>
                                         <select name="tipo" id="tipo" class="form-control">
                                                  <option >Seleccione</option>
                                                  <option value="Marculino">Bitel</option>
                                                  <option value="Femenino">Claro</option>
                                                  <option value="Femenino">Entel</option>
                                                  <option value="Femenino">Movistar</option>
            
            
                                         </select>

                                    </div>                                              
                             </div> 
                        </div>
                        <div class="row">
                        <div class="col-sm-8">
                                  <div class="form-group">
                                   <label for="nombres">Direccion</label>
                                        <input type="text" name="direccion" id="direccion" class="form-control" placeholder="Ingrese Direccion"> 
                                   </div>                               
                             </div>  
                        </div>
                     <div class="row">
                        <div class="col-sm-4">
                                    <div class="form-group">
                                       <label>Distrito</label>
                                         <select name="distrito" id="distrito" class="form-control">
                                                  <option >Seleccione</option>
                                                  <option value="Marculino">Bitel</option>
                                                  <option value="Femenino">Claro</option>
                                                  <option value="Femenino">Entel</option>
                                                  <option value="Femenino">Movistar</option>
                                         </select>

                                    </div>                                              
                             </div> 

                           
                                    <div class="col-sm-4">
                                            <div class="form-group">
                                            <label>Provincia</label>
                                                <select name="provincia" id="provincia" class="form-control">
                                                        <option >Seleccione</option>
                                                        <option value="Marculino">Bitel</option>
                                                        <option value="Femenino">Claro</option>
                                                        <option value="Femenino">Entel</option>
                                                        <option value="Femenino">Movistar</option>
                                                </select>

                                            </div>                                              
                                    </div> 
                                    <div class="col-sm-4">
                                            <div class="form-group">
                                            <label>Departamento</label>
                                                <select name="departamento" id="departamento" class="form-control">
                                                        <option >Seleccione</option>
                                                        <option value="Marculino">Bitel</option>
                                                        <option value="Femenino">Claro</option>
                                                        <option value="Femenino">Entel</option>
                                                        <option value="Femenino">Movistar</option>
                                                </select>

                                            </div>                                              
                                    </div> 
                             
                             </div>


                        
                             <div class="row">
                                 <div class="col-sm-4"> 
                                    <label for="" class="control-label" style="font-size: 13px;color: #676a6c">
                                            
                                    
                                    <div style="margin-top: 20px" class="from-group ">

                                        <button  id="save" class="btn btn-primary btn-sm" type="button"><i class="far fa-save"></i> Guardar</button>
                                        <button class="btn btn-danger" type="reset">Limpiar</button>
                                    </div>
                                </div>
                            </div>

                                    </div>
                                 </div>
                             </div>
                          </div>
                 
                      </div>                            
                </div>
                     
                        

</section>




@endsection