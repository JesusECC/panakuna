@extends('productos.principal')
@section ('contenido')

<section class="content-header">
    <h1 style="margin-top: 55px;">
        Panel de Administrador
        <small>Version 2.3.0</small>
    </h1>
    <ol class="breadcrumb" style="margin-top: 55px;">
        <li>
            <a href="#">
                <i class="fas fa-dolly"></i> Usuarios de </a>
        </li>
        <li class="active">Panakuna</li>
        <li>
            <a href="#">
                Nuevo</a>
        </li>
    </ol>

</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box" style="border-top: 3px solid #18A689">
                <div class="box-header with-border" style="padding: 10px !important">
                    <h4>
                        <strong style="font-weight: 400">
                            <i class="fas fa-dolly"></i> Registro Usuarios Panakuna
                        </strong>
                    </h4>
                    @if(count($errors)>0)
                    <div class="alert-alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach

                        </ul>
                    </div>
                    @endif
                </div>

                <!-- mantener valores al -->

                <div class="box-body bg-gray-c">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="panel panel-default panel-shadow">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="" class="control-label" style="font-size: 13px;color: #676a6c">
                                            Registrar usuario
                                        </label>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="nombres">Email</label>
                                                <input type="text" name="email" id="email" class="form-control"
                                                    placeholder="Ingrese email">
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="nombres">Password</label>
                                                <input type="text" name="pass" id="pass" class="form-control"
                                                    placeholder="Ingrese Contraseña">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Roles</label>
                                                <select name="roles" id="roles" class="form-control">
                                                <option value="" disabled="" selected="">Seleccione</option>
                                                    @foreach($roles as $rol)
                                                    <option value="{{$rol->id}}">{{$rol->nombre}}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>
                                       
                                    </div>                                     
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label for="" class="control-label" style="font-size: 13px;color: #676a6c">
                                                <div style="margin-top: 20px" class="from-group ">
                                                    <button id="save" class="btn btn-primary btn-sm" type="button"><i
                                                            class="far fa-save"></i> Guardar</button>
                                                    <button class="btn btn-danger" type="reset">Limpiar</button>
                                                </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
</section>




@push('scripts')

<script type="text/javascript">
    $('#save').click(function () {
        saveUser();
    });

    function saveUser() {
        let email = $("#email").val();
        // console.log(codigo);
        let pass = $("#pass").val();
        let rol = $("#roles").val();
        let roles = parseFloat(rol);   

        var dat='';
        dat = [{
            email: email, pass: pass, roles: roles
        }];

        console.log(dat);

        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: { datos: dat },
            url: '/guardaruser',
            type: 'post',
            dataType: "json",
            beforeSend: function () {
                console.log("------------------------------")
            },
            success: function (response) {
                console.log(response);
                console.log(response.veri);
                if (response.veri == true) {
                    alert("retorno");
                      var urlBase=window.location.origin;
                      var url=urlBase+'/'+response.data;
                      document.location.href=url;
                } else {
                    alert("problemas al guardar la informacion");
                }
            }
        });
    }

</script>

@endpush

@endsection