
<div class="modal fade in" aria-labelledby="myModalLabel" role="dialog" tabindex="-2" id="modal-create-personal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header mh-c" style="border:1px solid #18A689 !important;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <img src="{{asset('iconos-svg/profile.svg')}}" alt="" width="60px">
      </div>
      <div class="modal-body">
        <div class="box box-success">
          <div class="row" style="margin-top:5px;margin-right:2.5px;margin-left:2.5px;">
            <div class="col-md-6">
              PANAKUNA
            </div>
          </div>
          <div class="box-header with-border" style="padding: 10px !important">
            <center>
                <h4 class="box-title" style="font-size: 14px !important;text-align: center;color: #676a6c !important;">
                    REGISTRAR PRODUCTO 
                </h4>
            </center> 
          <div class="box-body">
          <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Registro 
                        </div>
                        <div class="panel-body">
                            <div class="row">
                               <div class="row">
                                    <div class="col-lg-12" style="margin-bottom:5px;">
                                        <div class="form-group">
                                            <label for="email" class="col-md-2 control-label">Codigo:</label>
                                            <div class="col-md-3">
                                            <input type="text" name="codigo" id="codigo" class="form-control" placeholder="Codigo Producto"> 
                                            </div>
                                            <label for="email" class="col-md-2 control-label">Nombre:</label>
                                            <div class="col-md-5">
                                            <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Ingrese Nombre Producto"> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12" style="margin-bottom:5px;">
                                        <div class="form-group">
                                            <label for="email" class="col-md-2 control-label">Precio:</label>
                                            <div class="col-md-5">
                                            <input type="number" name="precio" id="precio" class="form-control" placeholder="Ingrese Precio"> 
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="col-lg-12" style="margin-bottom:5px;">
                                        <div class="form-group">
                                            <label for="fecha_nacimiento"  class="col-md-2 control-label">Peso</label>
                                            <div class="col-lg-3">
                                            <input type="number" name="peso" id="peso" class="form-control" placeholder="Ingrese Nombres"> 
                                            </div>
                                            <label for="fecha_inicio"  class="col-md-2 control-label">Cantidad</label>
                                            <div class="col-lg-3">
                                            <input type="number" name="cantidad" id="cantidad" class="form-control" placeholder="Ingrese Cantidad"> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12" style="margin-bottom:5px;">
                                        <div class="form-group">
                                            <label for="fecha_nacimiento"  class="col-md-2 control-label">Tipo Producto</label>
                                            <select name="tipo" id="tipo" class="form-control">
                                                  <option >Seleccione</option>
                                                  <option value="1">Cafe Organico</option>
                                                  <option value="2">Frutas hidratada</option>
                                                  <option value="3">Stack Saludables</option>
                                                  <option value="4">Suplementos</option>
                                         </select>
                                           
                                            
                                        </div>
                                    </div>
                                    <div class="col-lg-12" style="margin-bottom:5px;">
                                        <div class="form-group">
                                            <label for="fecha_nacimiento"  class="col-md-2 control-label">Descripcion</label>
                                            <input type="txt" name="descripcion" id="descripcion" class="form-control" placeholder="Ingrese Descripcion del producto"> 
    
                                            
                                        </div>
                                    </div>
                                    <div class="col-lg-12" style="margin-bottom:5px;">
                                        <div class="form-group">
                                            <label for="fecha_nacimiento"  class="col-md-2 control-label">Descuento</label>
                                            <input type="number" name="descuento" id="descuento" class="form-control" placeholder="Ingrese Descripcion del producto"> 
                                        </div>
                                    </div>
                                    <div class="col-lg-12" style="margin-bottom:5px;" >
                                        <div class="form-group">
                                            <label for="codigo"  class="col-md-2 control-label">Imagen Producto</label>
                                            <input  type="file" id="files" name="foto[]" class="form-control">
                                            <br>
                                            <output id="list">
                                            </output>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button  id="save" class="btn btn-primary btn-sm" type="button"><i class="far fa-save"></i> Guardar</button>
       <button type="button" class="btn btn-danger"  data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i> Cerrar</button>
      </div>
    </div>
  </div> 
</div>
</div>









