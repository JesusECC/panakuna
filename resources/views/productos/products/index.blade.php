@extends('productos.principal')
@section ('contenido')
<section class="content-header">
	<h1 style="margin-top: 55px;">
		Panel de Administrador
		<small>Version 1.0.0</small>
    </h1>
    <ol class="breadcrumb" style="margin-top: 55px;">
    	<li>
    		<a href="#">
    			<i class="fas fa-dolly"></i> Lista </a>
    	</li>
    	<li class="active">Listado de Producto </li>
    </ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border" style="padding: 10px !important">
					<h4>
						<strong style="font-weight: 400">
							<i class="fas fa-list-ul"></i> Lista de Productos Panakuna
						</strong>
					</h4>
					<div class="ibox-title-buttons pull-right">
                        <a  href="{{route('products-create')}}" style="text-decoration: none !important">
                            <button class="btn btn-block btn-success" style="background-color: #18A689 !important;">
                                <i class="fas fa-plus-circle" ></i> Nuevo productos
                            </button>
                        </a>
                    </div>
				</div>
              
				<div class="box-body">
					<table id="example" class="table table-striped table-bordered table-hover" style="width:100%;font-size: 11px !important">
				       <thead>
				            <tr>
				                <th>Codigo</th>
				                <th>Nombre</th>
				                <th>Precio</th>
				                <th>Descripcion</th>
				                <th>Cantidad</th>
				               

                                <th>imagen</th>
				                <th>Opciones</th>
				            </tr>
				        </thead>
                        
				        <tbody>
                        @foreach($producto as $pro)
				        	<tr>
				        		<td>
								
				        			{{$pro->codigo}}
				        		</td>
				        		<td>
				        			{{$pro->nombre}}
				        		</td>
				        		<td>
				        			{{$pro->precio}}
				        		</td>
				        		<td>
				        			{{$pro->descripcion}}
				        		</td>
				        		<td>
				        			 {{$pro->stock}}
				        		</td>
                            
                                <td>
				        			<img  src="{{ $pro->nombre_imagen}}" style="margin-left:30%" width="30px">
				        		</td>
				        		<td align="center">
				        			<a  href=""  data-target="#modal-show-{{$pro->id}}"  data-toggle="modal" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="left" title="Ver Producto"><i class="far fa-eye"></i> </a>
									<a href="{{route('producto-editproducto',$pro->id)}}" class="btn btn-success btn-xs" role="button"><i class="fas fa-edit" title="Editar Producto"></i> </a>
									<a href="" data-target="#modal-delete-{{$pro->id}}" data-toggle="modal" class="btn btn-danger btn-xs" title="Eliminar Producto"><i class="fas fa-trash-alt"></i> </a>
								</td>
							</tr>
							
						@include('productos.products.modaleliminar')
						@include('productos.products.modalver')
						@endforeach
				        </tbody>
    				</table> 			
				</div>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
</section><!-- /.content -->
@push('scripts')

<script type="text/javascript">

</script>
@endpush
@endsection