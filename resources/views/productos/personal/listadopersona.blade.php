@extends('productos.principal')
@section ('contenido')
<section class="content-header">
	<h1 style="margin-top: 55px;">
		Panel de Administrador
		<small>Version 2.3.0</small>
    </h1>
    <ol class="breadcrumb" style="margin-top: 55px;">
    	<li>
    		<a href="#">
    			<i class="fas fa-dolly"></i> Lista </a>
    	</li>
    	<li class="active">Listado de Clientes </li>
    </ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border" style="padding: 10px !important">
					<h4>
						<strong style="font-weight: 400">
							<i class="fas fa-list-ul"></i> Listado de Personas Registradas
						</strong>
					</h4>
					<div class="ibox-title-buttons pull-right">
						<a href="" style="text-decoration: none !important">
							<button class="btn btn-block btn-success" style="background-color: #18A689 !important;">
								<i class="fas fa-plus-circle"></i> Nuevo Usuario
							</button></a>
					</div>
				</div>
              
				<div class="box-body">
					<table id="example" class="table table-striped table-bordered table-hover" style="width:100%;font-size: 11px !important">
				       <thead>
				            <tr>
				                <th>ID</th>
				                <th>Nombres y Apellidos</th>
				                <th>DNI</th>
								<th>E-mail</th>
				                <th>Opciones</th>
				            </tr>
				        </thead>
				        <tbody>
				        @foreach($cliente_persona as $persona)
				        	<tr>
				        		<td>
				        			{{$persona->id}}
				        		</td>
				        		<td>
				        			{{$persona->nombre}}
				        		</td>
				        		<td>
				        			{{$persona->numero_documento}}
				        		</td>
								<td>
				        			{{$persona->email}}
				        		</td>
				        		<td align="center">
				        			<a  href=""  data-target="#modal-show-{{$persona->id}}"  data-toggle="modal" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="left" title="Ver Persona"><i class="far fa-eye"></i> </a>
									<a href="{{route('personal-editPersonal',$persona->id)}}" class="btn btn-success btn-xs" role="button"><i class="fas fa-edit" title="Editar Persona"></i> </a>
									<a href="" data-target="#modal-delete-{{$persona->id}}"  data-toggle="modal" class="btn btn-danger btn-xs" title="Eliminar Persona"><i class="fas fa-trash-alt"></i> </a>
								</td>
							</tr>
							@include('productos.personal.vermodalPersona')
							@include('productos.personal.modaleliminar')
							@endforeach
				        </tbody>
				       
    				</table>
    				
				</div>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
</section><!-- /.content -->

@push('scripts')


<script type="text/javascript">

</script>
@endpush


@endsection