<div class="modal fade in" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal-show-{{$persona->id}}" style="border-radius:0px !important;">
  <div class="modal-dialog " role="document">
  	<div class="modal-content">
	      <div class="modal-header mh-v">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
	        <i class="far fa-eye modal-icon"></i>
	      </div>
	  <div class="modal-body">
	  	<div class="row" style="margin-bottom: 10px;">
	  		<div class="col-sm-12">
	  			<p class="modal-title" id="myModalLabel" style="color: #0F7A87;font-size: 16px;text-align: center;">{{$persona->id}}</p>
	  		</div>
	  	</div>
	  	<div class="row">	
	  		<div class="col-lg-4">
	  		
	  		</div><!--final de columna de foto-->
	  		<div class="col-lg-8">
	  			<p>
	  				<span><i class="fas fa-user-circle"></i>s Nombre Completos  : {{$persona->nombre}}</span> 
	  			</p>
	  			<p>
	  				<span><i class="fas fa-file"></i> DNI : {{$persona->numero_documento}}</span>
	  			</p>
	  			<p>
	  				<span><i class="fas fa-pen-alt"></i> Email : {{$persona->email}}</span>
	  			</p>	
	
	  		</div><!--final de cuerpo-->
	  	</div>	
	  </div>
  <div class="modal-footer">
       <button type="button" class="btn btn-danger"  data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i> Cerrar</button>
  		</div>

  	</div>

  </div> 


</div>
<!-- Modal -->