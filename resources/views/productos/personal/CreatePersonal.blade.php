@extends('layout.admin2')
@section('contenido')
<div class="wrap-login102" style="align:center !important; ">
				<div class="login100-form-title" style="background-image: url(images/registrarpanakunacabezal.png)">
					<span class="login100-form-title-1">Registrar Persona 
						
					</span>
				</div>
</div>
	<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box" style="border-top: 3px solid #fff">
                <div class="box-header with-border" style="padding: 10px !important">
                    <h4>
                        <strong style="font-weight: 400">
                            <i class="fas fa-user-plus"></i></i>
                        </strong>
                    </h4>
                    @if(count($errors)>0)
                    <div class="alert-alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach

                        </ul>
                    </div>
                    @endif
                </div>
                <!-- mantener valores al -->
                <div class="box-body bg-gray-c">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="panel panel-default panel-shadow">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="" class="control-label" style="font-size: 13px;color: #676a6c">
                                            Registrar
                                        </label>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="nombres">Nombres</label>
                                                <input type="text" name="nombre" id="nombre" class="form-control"
                                                    placeholder="Ingrese el Nombre">
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="nombres">Apellidos</label>
                                                <input type="text" name="apellido" id="apellido" class="form-control"
                                                    placeholder="Ingrese Apellido">
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="nombres">DNI</label>
                                                <input type="text" name="numero_documento" id="numero_documento" class="form-control"
                                                    placeholder="Ingrese DNI">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                        <div class="form-group">
													<label>
														Fecha de Nacimiento
													</label>
													<div class="input-group date">
													    <div class="input-group-addon">
															<i class="far fa-calendar-alt"></i>
														</div>
												<input  id="fecha_nacimiento" type="date" class="form-control pull-right" name="fecha_nacimiento">	
												    </div>
											    </div>										
											</div>		
                                       <!-- <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="nombres">E-mail</label>
                                                <input type="text" name="email" id="email" class="form-control"
                                                    placeholder="Ingrese E-mail">
                                            </div>
                                        </div>-->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="nombres">Telefono</label>
                                                <input type="text" name="telefono" id="telefono"
                                                    class="form-control" placeholder="Ingrese  telefonico">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">                            
                                         
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Tipo Telefono</label>
                                                <select name="tipotelefono" id="tipotelefono" class="form-control">
                                                <option value="" disabled="" selected="">Seleccione</option>
                                                    @foreach($tipo_telefono as $tt)
                                                    <option value="{{$tt->idtipo_telefono}}">{{$tt->telefono}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Operador</label>
                                                <select name="operador" id="operador" class="form-control">
                                                <option value="" disabled="" selected="">Seleccione</option>
                                                  @foreach($operador as $oper)
                                                    <option value="{{$oper->idoperador}}">{{$oper->operador}}</option>
                                                  @endforeach
                                                </select>
                                            </div>
                                        </div>    

                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <label for="nombres">Direccion</label>
                                                <input type="text" name="direccion" id="direccion" class="form-control"
                                                    placeholder="Ingrese direccion">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                    
                                    <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Departamento</label>
                                            <select  class="form-control selectpicker" id="departamento" name="departamento" data-live-search="true">
                                            <option value="" disabled="" selected="">Seleccione</option>
                                            @foreach($departamento as $depa)                
                                            <option value="{{$depa->iddepartamento}}">{{$depa->departamento}}</option>
                                            @endforeach  
                                            </select>   
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                    <label class="control-label">Provincia</label>                                  
                                                <select  class="form-control selectpicker" id="provincia" name="provincia" data-live-search="true">
                                                    <option value="" disabled="" selected="">Seleccione</option>
                                                </select>   
                                                </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                    <label class="control-label">Distrito</label>                                  
                                                <select  class="form-control selectpicker" id="distrito" name="distrito" data-live-search="true">
                                                    <option value="" disabled="" selected="">Seleccione</option>
                                                </select>   
                                            </div>
                                        </div>
                                       
      <div class="col-md-4">
        <div class="panel panel-default panel-shadow">
            <div class="panel-body">
               <div class="box-body">


                        <label for="" class="control-label" style="font-size: 18px;color: #676a6c"> CUENTA DE USUARIO  </label>
                                    <div class="row"> 
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                            <label for="sueldo">Usuario</label>
                                                <input type="text" name="email" id="email" class="form-control" placeholder="Ingrese nombre de Usuario">
                                            </div>  
                                        </div>                                  
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                        <label for="fecha_inicio">Contraseña</label>
                                                        <input type="text" name="password" id="pw" class="form-control" placeholder="Ingrese contraseña"> 
                                                    </div>  
                                         </div> 
                                   <div style="margin-top: 20px" class="from-group ">

                                            <button  id="save" class="btn btn-primary btn-sm" type="button"><i class="far fa-save"></i>Registrar</button>
                                            <button class="btn btn-danger" type="reset">Limpiar</button>
            
                                   </div>
                                     </div>
                                    </div>  
                                </div>                 
                            </div>                            
                        </div> 
        
                                
</section>
@push('scripts')
<script type="text/javascript">
    $('#save').click(function () {


        savePersona();
});

    // $('#departamento').click(function(){
    //         console.log('departamento');
    // });
    // $("#departamento").change(console.log('entre'));
    var selectDepartamento = document.getElementById('departamento');
    selectDepartamento.addEventListener('change',function(){
        var selectedOption = this.options[selectDepartamento.selectedIndex];
        console.log(selectedOption.value + ': ' + selectedOption.text);
        var id=selectedOption.value;
        console.log(id);
        provincia(id);
        
    });
    var selectProvincia = document.getElementById('provincia');
    selectProvincia.addEventListener('change',function(){
        var selectedOption = this.options[selectProvincia.selectedIndex];
        console.log(selectedOption.value + ': ' + selectedOption.text);
        var id=selectedOption.value;
        console.log(id);
        distrito(id);        
    });
    function provincia(idDepartamento){
        console.log(idDepartamento,'-----');
      $.ajax({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data:{departamento:idDepartamento}, //datos que se envian a traves de ajax
            url:'/departamento', //archivo que recibe la peticion
            type:'post', //método de envio
            dataType:"json",//tipo de dato que envio 
            beforeSend: function () {
                console.log('procesando');
                // $("#resultado").html("Procesando, espere por favor...");
            },
            success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                if(response.veri==true){
                    console.log(response.provincia);
                    // var urlBase=window.location.origin;
                    // var url=urlBase+'/'+response.data;
                    // document.location.href=url;
                    var provincia=response.provincia;
                    var va;
                    // console.log(response.provincia,response.veri);
                    va='<option value="" disabled="" selected="">Seleccione</option>'
                    for(const i in provincia){
                        va+='<option value="'+provincia[i]['idprovincia']+'">'+provincia[i]['provincia']+'</option>';                 
                    }
                    $("#provincia").html(va); 
                }else{
                    alert("problemas al enviar la informacion");
                }
            }
        });
    }
    function distrito(idProvincia){
        console.log(idProvincia,'-----');
      $.ajax({
            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data:{Provincia:idProvincia}, //datos que se envian a traves de ajax
            url:'distrito', //archivo que recibe la peticion
            type:'post', //método de envio
            dataType:"json",//tipo de dato que envio 
            beforeSend: function () {
                console.log('procesando');
                // $("#resultado").html("Procesando, espere por favor...");
            },
            success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                if(response.veri==true){
                    console.log(response.distrito);
                    // var urlBase=window.location.origin;
                    // var url=urlBase+'/'+response.data;
                    // document.location.href=url;
                    var distrito=response.distrito;
                    var va;
                    // console.log(response.distrito,response.veri);
                    va='<option value="" disabled="" selected="">Seleccione</option>'
                    for(const i in distrito){
                        va+='<option value="'+distrito[i]['iddistrito']+'">'+distrito[i]['distrito']+'</option>';      
                    }
                    $("#distrito").html(va); 
                }else{
                    alert("problemas al enviar la informacion");
                }
            }
        });
    }


    

        function savePersona() {
        let nombre = $("#nombre").val();
        // console.log(codigo);
        let apellido = $("#apellido").val();
        let numero_documento = $("#numero_documento").val();
        let fecha_nacimiento = $("#fecha_nacimiento").val();
        let telefono = $("#telefono").val();
        let tipotele = $("#tipotelefono").val();
        let tipotelefono=parseInt(tipotele);

        let ope = $("#operador").val();
        let operador=parseInt(ope);

        let direccion = $("#direccion").val();

        let depar = $("#departamento").val();
        let departamento=parseInt(depar);
        
        let provi = $("#provincia").val();
        let provincia=parseInt(provi);

        let dist = $("#distrito").val();
        let distrito=parseInt(dist);

        /* PARA EL LOGEO */

        let email = $("#email").val();
        let pw = $("#pw").val();

        var dat='';

        dat=[{
            nombre: nombre,apellido: apellido,numero_documento: numero_documento,fecha_nacimiento: fecha_nacimiento,
            telefono: telefono,tipotelefono: tipotelefono,operador: operador,direccion: direccion,
            departamento: departamento,provincia: provincia,distrito: distrito,email: email,pw: pw}];

        console.log(dat);

        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: { datos: dat },
            url: '/guardarpersonal',
            type: 'post',
            dataType: "json",
            beforeSend: function () {
                console.log("------------------------------")
            },
            success: function (response) {
                console.log(response);
                console.log(response.veri);
                if (response.veri == true) {
                  // alert("retorno");
                      var urlBase=window.location.origin;
                      var url=urlBase+'/'+response.data;
                      document.location.href=url;
                } else {
                    alert("problemas al guardar la informacion");
                }
            }
        });
    
    }




</script>
@endpush
@endsection