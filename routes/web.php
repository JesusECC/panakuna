<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
Route::get('/', functi on () {
    return view('welcome');
});


*/


Route::get('/','MainController@index');
Route::get('/cart','CartController@index');//carrito de comprar
// Route::get('/login','LoginController@index');
// Route::get('/login','LoginController@index');

Route::get('contactos',['as'=>'contactos','uses'=>'ContactsController@index']);


Route::get('/detalleproducto','DetalleproductoController@index');
Route::get('olvidarcuenta',['as'=>'olvidarcontra','uses'=>'OlvidarcuentaController@index']);

Route::get('/dashboard','DashboardController@index');//menu principal para el administrador


/* Cliente empresa*/
Route::get('empresa',['as'=>'empresa','uses'=>'EmpresaController@index']);
Route::get('/empresa/create',['as'=>'createempresa','uses'=>'EmpresaController@create']);
Route::post('/guardarempresa','EmpresaController@store');
Route::post('/departamento','EmpresaController@provincia');
Route::post('/distrito','EmpresaController@distrito');
Route::delete('empresa/eliminar/{id}',['as'=>'empresa-eliminar','uses'=>'EmpresaController@destroy']);

Route::get('empresa/{id}/edit',['as'=>'empresa-editEmpresa','uses'=>'EmpresaController@edit']);
Route::post('/updateEmpresa','EmpresaController@update');



/* Productos
Route::get('/productos/create',['as'=>'products-create','uses'=>'ProductsController@create']);

*/
Route::get('/productos',['as'=>'producto','uses'=>'ProductsController@index']);
Route::get('/productos/create',['as'=>'products-create','uses'=>'ProductsController@create']);
Route::post('/guardarproducto','ProductsController@store');
Route::get('productos/{id}/edit',['as'=>'producto-editproducto','uses'=>'ProductsController@edit']);
Route::post('/update','ProductsController@update');
Route::delete('producto/eliminar/{id}',['as'=>'producto-eliminar','uses'=>'ProductsController@destroy']);


/** usuario Panakuna */
Route::get('/usuarios',['as'=>'usuarios','uses'=>'UsuariosController@index']);
Route::get('/usuarios/create',['as'=>'usuarios-create','uses'=>'UsuariosController@create']);
Route::post('/guardaruser','UsuariosController@store');


/****personal****/
Route::get('personal',['as'=>'personal','uses'=>'PersonalController@index']);
Route::get('/registrar',['as'=>'registrarpersona','uses'=>'PersonalController@create']);//registrar los usuarios
Route::post('/guardarpersonal','PersonalController@store');
Route::post('/departamento','PersonalController@provincia');
/*Route::post('personal/departamento',['as'=>'personal-departamento','uses'=>'PersonalController@provincia']);
Route::post('personal/distrito',['as'=>'personal-distrito','uses'=>'ClienteController@distrito']);
*/
Route::post('/distrito','PersonalController@distrito');
Route::delete('persona/eliminar/{id}',['as'=>'persona-eliminar','uses'=>'PersonalController@destroy']);
Route::get('persona/{id}/edit',['as'=>'personal-editPersonal','uses'=>'PersonalController@edit']);
//Route::get('/dashboard/productos/editarproductos','ProductsController@index');
Route::post('/updatepersona','PersonalController@update');


/* cliente  Empresa*/
//Route::get('/dashboard/personal/clienteempresa','ClienteempresaController@index');

Route::get('galeria',['as'=>'galeria','uses'=>'AboutController@index']);// galeria de los productos en general
//carrio de compras
Route::get('main/cart',['as' => 'cart-show','uses'=>'CartController@show']);
Route::get('compras/add/{cart}',['as' => 'cart-add','uses'=>'CartController@add']);
Route::get('main/update/{id}/{canti?}',['as' => 'cart-update','uses'=>'CartController@update']);
Route::get('main/delete/{id}',['as' => 'cart-delete','uses'=>'CartController@delete']);
Route::get('main/trash',['as' => 'cart-trash','uses'=>'CartController@trash']);
// Enviamos nuestro pedido a PayPal
Route::get('main', 'PaymentController@index');
Route::post('main/paypal', 'PaymentController@payWithpaypal');
// Después de realizar el pago Paypal redirecciona a esta ruta
Route::get('main/status', 'PaymentController@getPaymentStatus');

/********************PDF comprobante */
Route::get('carrito/pdf',['as'=>'cart-pdf','uses'=>'CartController@pdf']);



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
