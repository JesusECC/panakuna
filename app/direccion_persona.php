<?php

namespace panakuna;

use Illuminate\Database\Eloquent\Model;

class direccion_persona extends Model
{
    protected $table='direccion_persona';
    protected $primaryKey='id';
    public $timestamps=false;


    protected $filleable = [

    	'direccion',
    	'persona_id',
    	'estado_P',
    	'distrito_iddistrito',
        'distrito_provincia_idprovincia',
        'distrito_provincia_departamento_iddepartamento',
     
   ];

   protected $guarded =[
     
];
}
