<?php

namespace panakuna;

use Illuminate\Database\Eloquent\Model;

class detalle_pedido extends Model
{
    protected $table='detalle_pedido';
    protected $primaryKey='id';
    public $timestamps=false;


    protected $filleable = [

    	'precio',
    	'cantidad',
    	'producto_id',
    	'pedido_id',
    	
     
   ];

   protected $guarded =[
     
];
}
