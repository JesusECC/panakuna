<?php

namespace panakuna;

use Illuminate\Database\Eloquent\Model;

class tipo_telefono extends Model
{
    protected $table='tipo_telefono';
    protected $primaryKey='idtipo_telefono';
    public $timestamps=false;


    protected $filleable = [
            'idtipo_telefono',
            'telefono',
    	
     
   ];

   protected $guarded =[
     
];
}
