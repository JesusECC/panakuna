<?php

namespace panakuna;

use Illuminate\Database\Eloquent\Model;

class imagen extends Model
{
    protected $table='imagen';
    protected $primaryKey='id';
    public $timestamps=false;


    protected $filleable = [
            'nombre_imagen',
            'producto_id',
    	
     
   ];

   protected $guarded =[
     
];
}
