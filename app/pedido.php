<?php

namespace panakuna;

use Illuminate\Database\Eloquent\Model;

class pedido extends Model
{
    protected $table='pedido';
    protected $primaryKey='id';
    public $timestamps=false;


    protected $filleable = [

    	'serie_proforma',
    	'numero_proforma',
    	'nombre',
    	'numero_ruc_dni',
    	'fecha',
    	'total',
    	'descripcion',
    	'plazo_oferta',
    	'subtotal',
    	'cliente_id',
    	'tipo_comprobante',
    	'estado_idestado',
     
   ];

   protected $guarded =[
     
];
}
