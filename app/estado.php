<?php

namespace panakuna;

use Illuminate\Database\Eloquent\Model;

class estado extends Model
{
    protected $table='estado';
    protected $primaryKey='idestado';
    public $timestamps=false;


    protected $filleable = [

    	'nombre_estado',
    	
     
   ];

   protected $guarded =[
     
];
}
