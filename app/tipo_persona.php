<?php

namespace panakuna;

use Illuminate\Database\Eloquent\Model;

class tipo_persona extends Model
{
    protected $table='tipo_persona';
    protected $primaryKey='id';
    public $timestamps=false;


    protected $filleable = [
            'numero',
            'cliente_persona_id',
            'estado_telefono_persona',
            'tipo_telefono_idtipo_telefono',
            'operador_idoperador',
           
    	
     
   ];

   protected $guarded =[
     
];
}
