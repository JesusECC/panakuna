<?php

namespace panakuna;

use Illuminate\Database\Eloquent\Model;

class caritoCompras extends Model
{
    protected $primaryKey='id';
    public $timestamps=false;


    protected $filleable = [

    	'nombre',
    	'precio',
    	'descripcion',
    	'peso',
    	'stock',
    	'codigo',
    	'descuento',
        'tipo',
        'estado_producto',
        'empresa',
        'cantidad',
        'subtotal'
    	
     
   ];

   protected $guarded =[
     
];
}
