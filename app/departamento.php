<?php

namespace panakuna;

use Illuminate\Database\Eloquent\Model;

class departamento extends Model
{
    protected $table='departamento';
    protected $primaryKey='id';
    public $timestamps=false;


    protected $filleable = [

    'iddepartamento',
    'departamento',	
   ]; 
   protected $guarded =[
     
];

}
