<?php

namespace panakuna;

use Illuminate\Database\Eloquent\Model;

class telefono_empresa extends Model
{
    protected $table='telefono_empresa';
    protected $primaryKey='id';
    public $timestamps=false;


    protected $filleable = [

    	'numero',
    	'cliente_empresa_id',
    	'estadp_telefono_empresa',
        'tipo_telefono_idtipo_telefono',
        'operador_idoperador',
    	
     
   ];

   protected $guarded =[
     
];
}
