<?php

namespace panakuna;

use Illuminate\Database\Eloquent\Model;

class roles extends Model
{
    protected $table='roles';
    protected $primaryKey='id';
    public $timestamps=false;


    protected $filleable = [

    	'nombre',
    	'descripcion',
    	'estado_R',
    	
    	
     
   ];

   protected $guarded =[
     
];
}
