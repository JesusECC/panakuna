<?php

namespace panakuna;

use Illuminate\Database\Eloquent\Model;

class direccion_empresa extends Model
{
    protected $table='direccion_empresa';
    protected $primaryKey='id';
    public $timestamps=false;


    protected $filleable = [

    	'direccion',//tabla aparte
      	'estado',
    	'distrito_iddistrito',
    	'distrito_provincia_idprovincia',
    	'distrito_provincia_departamento_iddepartamento',
    	
     
   ];

   protected $guarded =[
     
];
}
