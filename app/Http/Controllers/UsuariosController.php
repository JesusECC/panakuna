<?php
namespace panakuna\Http\Controllers;

use Illuminate\Http\Request;
use panakuna\users;
use panakuna\user_roles;
use panakuna\cliente_persona;
use panakuna\tipo_telefono;
use panakuna\roles;
use panakuna\departamento;
use panakuna\provincia;
use panakuna\distrito;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

use Response;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
use Carbon\Carbon; 

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
            $usuarios=DB::connection('mysql')->select('call sp_listarUsuariosPractica');
            
                return view('productos.usuarios.index',["usuarios"=>$usuarios]);        

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles=DB::table('roles')
        ->get();
        //
        return view('productos.usuarios.create',["roles"=>$roles]);        


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $email;
            $pass;
            $idroles;
           
            
            foreach($request->datos as $dato)
            {
                $email=$dato['nombre_RazonSocial'];
                $pass=$dato['ruc'];
                $idroles=$dato['roles'];
               
              
               
            } 
           
            $usuarios=DB::connection('mysql')->select('call  sp_registrarUsuarioPractica(?,?,?)',

            array($email,$pass,$idroles));

            //$TrabajadorHorario=DB::connection('sqlsrv3')->select('exec  sp_insertarHorarioTrabajador ?,?,?,?,?,?,?',array($Dni,$horario,$tipo_horario,$Empresa,$Codigo,$fecha_inicio,$estado));
            $veri=true;
            // return Response()->json(true);
            return ['data'=>'usuarios','veri'=>$veri];

        }catch(Exception $e)
        {
            return ['data'=>$e,'veri'=>$veri];
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
