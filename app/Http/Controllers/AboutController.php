<?php

namespace panakuna\Http\Controllers;

use Illuminate\Http\Request;

use DB;
class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Ventana de la Galeria de productos 
        if($request){
            $query = trim($request->get('searchText'));
            $producto=DB::connection('mysql')->select('call sp_listaproductos');
        }
    //     +"codigo": "ASDASD"
    // +"nombre": "VITALINTI-NUTRI MIX"
    // +"precio": "12.00"
    // +"descripcion": "NIBS de Cacao endulzados con Yacon , bañados en polvo de Lucuma."
    // +"stock": 5
    // +"tipo": 1
    // +"nombre_imagen": "hola"

        // dd($producto);
        return view('pages.home.catalago',["productos"=>$producto,"searchText"=>$query]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
