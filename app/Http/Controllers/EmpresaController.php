<?php

namespace panakuna\Http\Controllers;


use Illuminate\Http\Request;
use panakuna\users;
use panakuna\user_roles;
use panakuna\roles;
use panakuna\departamento;
use panakuna\provincia;
use panakuna\distrito;
use panakuna\operador;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

use Response;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
use Carbon\Carbon; 


class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request){
            $query=trim($request->get('searchText'));
            $empresa=DB::connection('mysql')->select('call sp_listausersempresa');
            
                return view('productos.empresa.index',["empresa"=>$empresa,"SearchText"=>$query]);        

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   $operador=DB::table('operador')
        ->get();
        $tipo_telefono=DB::table('tipo_telefono')
        ->get();
        $distrito=DB::table('distrito')
        ->get();
        $provincia=DB::table('provincia')
        ->get();
        $departamento=DB::table('departamento')
        ->get();


        return view ('productos.empresa.createEmpresa',["distrito"=>$distrito,"provincia"=>$provincia,"departamento"=>$departamento,"tipo_telefono"=>$tipo_telefono,"operador"=>$operador]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        try{
            $nombre_RazonSocial;
            $ruc;
            $direccion;
            $distrito;
            $provincia;
            $departamento;
            $telefono;
            $tipotelefono;
            $operador;
            $email;
            $pw;
            
            foreach($request->datos as $dato)
            {
                $nombre_RazonSocial=$dato['nombre_RazonSocial'];
                $ruc=$dato['ruc'];
                $direccion =$dato['direccion'];
                $distrito=$dato['distrito'];        
                $provincia =$dato['provincia'];
                $departamento=$dato['departamento'];
                $telefono=$dato['telefono'];
                $tipotelefono=$dato['tipotelefono'];
                $operador=$dato['operador'];
                $email=$dato['email'];
                $pw=Hash::make($dato['pw']);
              
               
            } 
           
            $empresa=DB::connection('mysql')->select('call  sp_registrousuarioempresa(?,?,?,?,?,?,?,?,?,?,?)',

            array($email,$pw,$nombre_RazonSocial,$ruc,$direccion,$distrito,$provincia,$departamento,$telefono,$tipotelefono,$operador));

            //$TrabajadorHorario=DB::connection('sqlsrv3')->select('exec  sp_insertarHorarioTrabajador ?,?,?,?,?,?,?',array($Dni,$horario,$tipo_horario,$Empresa,$Codigo,$fecha_inicio,$estado));
            $veri=true;
            // return Response()->json(true);
            return ['data'=>'/','veri'=>$veri];

        }catch(Exception $e)
        {
            return ['data'=>$e,'veri'=>$veri];
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipo_telefono=DB::table('tipo_telefono')
        ->get();
        $operador=DB::table('operador')
        ->get();
        $distrito=DB::table('distrito')
        ->get();
        $provincia=DB::table('provincia')
        ->get();
        $departamento=DB::table('departamento')
        ->get();
        $empresa=DB::connection('mysql')->select('call sp_ListarClienteEmpresaEdit(?)',array($id));
        //dd($empresa);

        return view('productos.empresa.editEmpresa',["empresa"=>$empresa,"tipo_telefono"=>$tipo_telefono,"operador"=>$operador,"distrito"=>$distrito,"provincia"=>$provincia,"departamento"=>$departamento]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            
            $nombre_RazonSocial;
            $ruc;
            $telefono;
            $direccion;
            $idtipotelefono;
            $idoperador;
            $distrito;
            $provincia;
            $departamento;
            $email;
            $pw;
            $id;
            $clienteid;
          
            
             foreach($request->datos as $dato){
                $clienteid=$dato['clienteid'];
                $id=$dato['id'];
                $nombre_RazonSocial=$dato['nombre_RazonSocial'];
                $ruc=$dato['ruc'];
                $direccion=$dato['direccion'];
                $distrito=$dato['distrito'];
                $provincia=$dato['provincia'];
                $departamento=$dato['departamento'];
                $telefono=$dato['telefono'];
                $idtipotelefono=$dato['tipotelefono'];
                $idoperador=$dato['operador']; 
               $email=$dato['email'];
                $pw=$dato['pw'];

                }
                $empresa=DB::connection('mysql')->select('call sp_update_Empresa(?,?,?,?,?,?,?,?,?,?,?,?,?)',
                array($nombre_RazonSocial,$ruc,$telefono,$idtipotelefono,$idoperador,$direccion,$departamento,$provincia,$distrito,$email,$pw,$id,$clienteid));
                
                return ['veri'=>true,'data'=>'/'];
        } catch (Exception $e) {

            return ['data'=>$e,'veri'=>$veri];
        }
  

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { //$producto=DB::connection('mysql')->select('call sp_EliminarProducto(?)',array($id));
       $empresa=DB::connection('mysql')->select('call sp_EliminarEmpresa(?)',array($id));
       return Redirect::to('empresa');
    }

/************************************************************************************************** */

public function provincia(Request $request)
{
    
    $idDepartamento=$request->get('departamento');
    $provincia=DB::connection('mysql')->select('call sp_listarprovincia(?)',array($idDepartamento));
    /*$provincia=DB::table('provincia')
    ->where('departamento_iddepartamento','=',$idDepartamento)
    ->get();*/
    // dd($request);
    return ['provincia' =>$provincia,'veri'=>true];
}
public function distrito(Request $request)
{
    
    $idProvincia=$request->get('Provincia');
    $distrito=DB::table('distrito')
    ->where('provincia_idprovincia','=',$idProvincia)
    ->get();
    // dd($request);
    return ['distrito' =>$distrito,'veri'=>true];

//    id
//    nombre_distrito
//    Provincia_idProvincia
//    Provincia_Departamento_idDepartamento
}
/**************************************************************************************************/
}
