<?php

namespace panakuna\Http\Controllers;

use Illuminate\Http\Request;
use panakuna\users;
use panakuna\user_roles;
use panakuna\cliente_persona;
use panakuna\tipo_telefono;
use panakuna\roles;
use panakuna\departamento;
use panakuna\provincia;
use panakuna\distrito;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

use Response;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
use Carbon\Carbon; 


class PersonalController extends Controller
{
    
    public function index(Request $request)
    {

        /*
        if($request){
           $query = trim($request->get('searchText'));
           $producto=DB::connection('mysql')->select('call sp_listarproducto');
           return view('productos.products.index',["producto"=>$producto,"searchText"=>$query]);
       }
        */
        if($request){
            $query=trim($request->get('searchText'));
            $cliente_persona=DB::connection('mysql')->select('call sp_listauserspersona');
            
                return view('productos.personal.listadopersona',["cliente_persona"=>$cliente_persona,"SearchText"=>$query]);        

        }
       

     
    }

   
    public function create()
    {
        $tipo_telefono=DB::table('tipo_telefono')
        ->get();
        $operador=DB::table('operador')
        ->get();
        $distrito=DB::table('distrito')
        ->get();
        $provincia=DB::table('provincia')
        ->get();
        $departamento=DB::table('departamento')
        ->get();


        return view ('productos.personal.CreatePersonal',["distrito"=>$distrito,"provincia"=>$provincia,"departamento"=>$departamento,"tipo_telefono"=>$tipo_telefono,"operador"=>$operador]);
        }

  
    public function store(Request $request)
    {
        
        $veri=false;
        try{
            $nombre;
            $apellido;
            $numero_documento;
            $fecha_nacimiento;
            $email;
            $telefono;
            $tipotelefono;
            $operador;
            $direccion;
            $departamento;
            $provincia;
            $distrito;
            $pw;
            
            foreach($request->datos as $dato)
            {
                $nombre=$dato['nombre'];
                $apellido=$dato['apellido'];
                $numero_documento =$dato['numero_documento'];
                $fecha_nacimiento=$dato['fecha_nacimiento'];        
                $telefono =$dato['telefono'];
                $tipotelefono=$dato['tipotelefono'];
                $operador=$dato['operador'];
                $direccion=$dato['direccion'];
                $departamento=$dato['departamento'];
                $provincia=$dato['provincia'];
                $distrito=$dato['distrito'];
                $email=$dato['email'];
                $pw=Hash::make($dato['pw']);
              
               
            } 
           
            $producto=DB::connection('mysql')->select('call  sp_registrousuariopersona(?,?,?,?,?,?,?,?,?,?,?,?,?)',

            array($email,$pw,$nombre,$apellido,$numero_documento,$fecha_nacimiento,$direccion,$distrito,$provincia,$departamento,$telefono,$tipotelefono,$operador));

            //$TrabajadorHorario=DB::connection('sqlsrv3')->select('exec  sp_insertarHorarioTrabajador ?,?,?,?,?,?,?',array($Dni,$horario,$tipo_horario,$Empresa,$Codigo,$fecha_inicio,$estado));
            $veri=true;
            // return Response()->json(true);
            return ['data'=>'/','veri'=>$veri];

        }catch(Exception $e)
        {
            return ['data'=>$e,'veri'=>$veri];
        }
        // return ['veri'=>$post];







    }

   
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $tipo_telefono=DB::table('tipo_telefono')
        ->get();
        $operador=DB::table('operador')
        ->get();
        $distrito=DB::table('distrito')
        ->get();
        $provincia=DB::table('provincia')
        ->get();
        $departamento=DB::table('departamento')
        ->get();
        $persona=DB::connection('mysql')->select('call sp_ListarPersonaEdit(?)',array($id));
        //dd($persona);
        return view('productos.personal.editPersonal',["persona"=>$persona,"tipo_telefono"=>$tipo_telefono,"operador"=>$operador,"distrito"=>$distrito,"provincia"=>$provincia,"departamento"=>$departamento]);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {

            $nombre;
            $apellido;
            $numero_documento;
            $fecha_nacimiento;
            $telefono;
            $tipotelefono;
            $operador;
            $direccion;
            $departamento;
            $provincia;
            $distrito;
            $email;
            $pw;
            $id;
            $clienteid;
            
            foreach($request->datos as $dato)
           {
               $id=$dato['id'];
               $clienteid=$dato['clienteid'];
               $nombre=$dato['nombre'];
               $apellido=$dato['apellido'];
               $numero_documento=$dato['numero_documento'];
               $fecha_nacimiento=$dato['fecha_nacimiento'];
               $telefono=$dato['telefono'];
               $tipotelefono=$dato['tipotelefono'];
               $operador=$dato['operador'];
               $direccion=$dato['direccion'];
               $departamento=$dato['departamento'];
               
               $provincia=$dato['provincia'];
               
               $distrito=$dato['distrito'];
               
               $email=$dato['email'];
               $pw=$dato['pw'];
           }
           $producto=DB::connection('mysql')->select('call sp_update_Persona(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
           array($nombre,$apellido,$numero_documento,$fecha_nacimiento,$telefono,$tipotelefono,$operador,$direccion,$departamento,$provincia,
                 $distrito,$email,$pw,$id,$clienteid));

           return ['veri'=>true,'data'=>'/'];
        } catch (Exception $e){
           
            return ['data'=>$e,'veri'=>$veri];
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       

        $cliente_persona=DB::connection('mysql')->select('call sp_EliminarPersonal(?)',array($id));
        return Redirect::to('personal');
    }

/************************************************************************************************** */

    public function provincia(Request $request)
    {
        
        $idDepartamento=$request->get('departamento');
      //  $provincia=DB::connection('mysql')->select('call sp_listarprovincia(?)',array($idDepartamento));
        $provincia=DB::table('provincia')
        ->where('departamento_iddepartamento','=',$idDepartamento)
        ->get();
        // dd($request);
        return ['provincia' =>$provincia,'veri'=>true];
    }
    public function distrito(Request $request)
    {
        
        $idProvincia=$request->get('Provincia');
        $distrito=DB::table('distrito')
        ->where('provincia_idprovincia','=',$idProvincia)
        ->get();
        // dd($request);
        return ['distrito' =>$distrito,'veri'=>true];

    //    id
    //    nombre_distrito
    //    Provincia_idProvincia
    //    Provincia_Departamento_idDepartamento
    }
/**************************************************************************************************/
}
