<?php

namespace panakuna\Http\Controllers;


use Illuminate\Http\Request;
use SistemaDigitalizacion\Trabajador;
use SistemaDigitalizacion\Horario_Detalle;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use panakuna\Http\Requests\ProductoFormRequest;
use panakuna\producto;
use Response;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
use Carbon\Carbon; 


class ProductsController extends Controller
{
   
    public function index(Request $request)
    {
        
       if($request){
           $query = trim($request->get('searchText'));
           $producto=DB::connection('mysql')->select('call sp_listaproductos');
      
           return view('productos.products.index',["producto"=>$producto,"searchText"=>$query]);
       }
       
    }

   
    public function create()
    {
       /*     $producto=db::table('producto')
            ->get();
            $detalle_pedido=db::table('detalle_pedido')
            ->get();
    */
            //return view('dashboard.products.create',["producto"=>$producto,"detalle_pedido"=>$detalle_pedido]);

            return view('productos.products.create');

    }

   
    public function store(Request $request)
    {
        // $post=$request->datos;
        $veri=false;
        try{
            $codigo;
            $nombre;
            $precio;
            $peso;
            $cantidad;
            $tipo;
            $descripcion;
            $descuento;
            $file;
            foreach($request->datos as $dato)
            {
                $codigo=$dato['codigo'];
                $nombre=$dato['nombre'];
                $precio=$dato['precio'];
                $peso=$dato['peso'];
                $cantidad=$dato['cantidad'];
                $tipo=$dato['tipo'];
                $descripcion=$dato['descripcion'];
                $descuento=$dato['descuento'];
                $file=$dato['file'];
               
            } 
           
            $producto=DB::connection('mysql')->select('call  sp_registroproducto(?,?,?,?,?,?,?,?,?)',

            array($nombre,$precio,$descripcion,$peso,$cantidad,$codigo,$descuento,$tipo,$file));

            //$TrabajadorHorario=DB::connection('sqlsrv3')->select('exec  sp_insertarHorarioTrabajador ?,?,?,?,?,?,?',array($Dni,$horario,$tipo_horario,$Empresa,$Codigo,$fecha_inicio,$estado));
            $veri=true;
            // return Response()->json(true);
            return ['data'=>'productos','veri'=>$veri];

        }catch(Exception $e)
        {
            return ['data'=>$e,'veri'=>$veri];
        }
        // return ['veri'=>$post];
    }

   
    public function show($id)
    {
        //

        
    }

   
    public function edit($id)
    {
        // solo es lista en el edit 
        // tengo que pasar un id
    
        $producto=DB::connection('mysql')->select('call sp_listarproductoEdit(?)',array($id));
        
        return view('productos.products.editproducto',["producto"=>$producto]);
    }

   
    public function update(Request $request)
    {

        /*$id=$request->get('id');
        $nombre=$request->get('nombre');
        
        return ['veri'=>true,'data'=>'$request->datos'];
        */
       // $idprueba=$request->get('datos2');
        try {

            $precio;
            $peso;
            $cantidad;
            $codigo;
            $descuento;
            $tipo;
            $descripcion;
            $file;
            foreach($request->datos as $dato)
           {
               $id=$dato['id'];
               $nombre=$dato['nombre'];
               $precio=$dato['precio'];
               $peso=$dato['peso'];
               $cantidad=$dato['cantidad'];
               $codigo=$dato['codigo'];
               $descuento=$dato['descuento'];
               $tipo=$dato['tipo'];
               $descripcion=$dato['descripcion'];
               $file=$dato['file'];
           }
           $producto=DB::connection('mysql')->select('call sp_update_producto(?,?,?,?,?,?,?,?,?,?)',array($nombre,$precio,$peso,$cantidad,$codigo,$descuento,$tipo,$descripcion,$file,$id));
           return ['veri'=>true,'data'=>'productos'];
        } catch (Exception $e){
           
            return ['data'=>$e,'veri'=>$veri];
        }

        
    }

   
    public function destroy($id)
    {
        $producto=DB::connection('mysql')->select('call sp_EliminarProducto(?)',array($id));
        return Redirect::to('productos'); 
    }
}
