<?php

namespace panakuna\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

use DB;
use panakuna\producto;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{

    public function __cosntruct(){
        if(!\Session::has('cart')) \Session::put('cart',array());
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('pages.cart.carritodecompra');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $cart= \Session::get('cart');
        if (isset($cart)) {
            $total=$this->total();
        }    
        return  view('pages.cart.carritodecompra',compact('cart','total'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id,$cantid)
    {
        //
        $cart= \Session::get('cart');
        $cart[$id]->cant=$cantid;
        //dd($cart);
        $cart[$id]->subto=$cart[$id]->precio*$cantid;
        \Session::put('cart',$cart);
        return redirect()->route('cart-show');
       //return dd($paquete,$cantid);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function add($id){
        $product=producto::find($id);

        $cart = \Session::get('cart');

        $product->cant=1;
        $product->subto=$product->precio;
        $cart[$product->id]=$product;
        \Session::put('cart',$cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');
        
        // dd($cart,!$cart);
        // if(!$cart) {
        //     $cart=[
        //         $id=>[
        //             "nombre"=>$product->nombre,
        //             "cantidad"=>1,
        //             "precio"=>$product->precio
        //         ]
        //     ];
        //     \Session::put('cart',$cart);
            
        //     return redirect()->back()->with('success', 'Product added to cart successfully!');
        // }
        // if(isset($cart[$id])) {
 
        //     $cart[$id]['cantidad']++;
 
        //     session()->put('cart', $cart);
 
        //     return redirect()->back()->with('success', 'Product added to cart successfully!');

        // }
        // $cart[$id] = [
        //     "nombre" => $product->nombre,
        //     "cantidad" => 1,
        //     "precio" => $product->precio
        // ];
        // session()->put('cart', $cart);        
        // return redirect()->back()->with('success', 'Product added to cart successfully!');
        // dd($cart);
        // $product->cant=1;
        // $product->subto=$productos->precio;
        // $cart[$product->id]=$productos;
        
        // dd($cart);
        // return  redirect()->route('galeria');
    }
    public function total(){
        $cart= \Session::get('cart');
        $total=0;
        
        foreach($cart as $t){ 
            $total+=$t->subto*$t->cant;
        }
        return  $total;
    }
    public function delete($id){
        $cart= \Session::get('cart');
        unset($cart[$id]);
        \Session::put('cart',$cart);
        return  redirect()->route('cart-show');
    }
    public function trash(){
        \Session::forget('cart');        
        return  redirect()->route('cart-show');
    }


    public function pdf(){

        $productos=DB::table('producto as p');

        $pdf=PDF::loadView('pages.cart.pdf',["producto"=>$productos]);
        return $pdf->stream('producto.pdf');

     
        
    
    }



}
