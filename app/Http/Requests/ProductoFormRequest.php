<?php

namespace panakuna\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'=>'required|max:100',
            'precio'=>'numeric',
            'descripcion'=>'required|max:100',
            'peso'=>'numeric',
            'stock'=>'numeric',
            'codigo'=>'required|max:100',
            'descuento'=>'numeric',
            'tipo'=>'numeric',
            'estado_producto'=>'numeric',
            'id'=>'require',
        ];
    }
}
