<?php

namespace panakuna;

use Illuminate\Database\Eloquent\Model;

class cliente_persona extends Model
{
    protected $table='cliente_persona';
    protected $primaryKey='id';
    public $timestamps=false;


    protected $filleable = [

    	'nombre',//tabla aparte
    	'apellido',
    	'numero_documento',
    	'fecha_nacimiento',
    	'email',
    	'estado_cliente_persona',
    	'celular',
    	'cliente_id',
     
   ];

   protected $guarded =[
     
];

}
