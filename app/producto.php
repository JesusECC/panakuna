<?php

namespace panakuna;

use Illuminate\Database\Eloquent\Model;

class producto extends Model
{
    
    protected $table='producto';
    protected $primaryKey='id';
    public $timestamps=false;


    protected $filleable = [

    	'nombre',//tabla aparte
    	'precio',
    	'descripcion',
    	'peso',
    	'stock',
    	'codigo',
    	'descuento',
        'tipo',
        'estado_producto',
    	'empresa',
    	
     
   ];

   protected $guarded =[
     
];
}
