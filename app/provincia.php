<?php

namespace panakuna;

use Illuminate\Database\Eloquent\Model;

class provincia extends Model
{ protected $table='provincia';
    protected $primaryKey='idprovincia';
    public $timestamps=false;


    protected $filleable = [
            'provincia',
            'departamento_iddepartamento',
    	
     
   ];

   protected $guarded =[
     
];
}
