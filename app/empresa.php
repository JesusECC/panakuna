<?php

namespace panakuna;

use Illuminate\Database\Eloquent\Model;

class empresa extends Model
{
    protected $table='empresa';
    protected $primaryKey='id';
    public $timestamps=false;


    protected $filleable = [
            'nombre',
            'mision',
            'vision',
            'historia',
            'descripcion',
            'premios',

            'correo',
            'telefono',
            'direccion',
            
    	
     
   ];

   protected $guarded =[
     
];
}
