<?php

namespace panakuna;

use Illuminate\Database\Eloquent\Model;

class distrito extends Model
{
    protected $table='distrito';
    protected $primaryKey='iddistrito';
    public $timestamps=false;


    protected $filleable = [

    	'distrito',
    	'provincia_idprovincia',
    	'provincia_departamento_iddepartamento',
    	
     
   ];

   protected $guarded =[
     
];
}
