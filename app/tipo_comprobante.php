<?php

namespace panakuna;

use Illuminate\Database\Eloquent\Model;

class tipo_comprobante extends Model
{
    protected $table='tipo_comprobante';
    protected $primaryKey='id';
    public $timestamps=false;


    protected $filleable = [
            'nombre',
            'descripcion',
    	
     
   ];

   protected $guarded =[
     
];
}
